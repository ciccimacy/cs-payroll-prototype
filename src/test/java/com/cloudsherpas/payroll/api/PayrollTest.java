package com.cloudsherpas.payroll.api;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.cloudsherpas.payroll.dto.PayrollDetails;

public class PayrollTest {
	private PayrollDetails payrollDetails = new PayrollDetails();

	@Test
	public void getSalaryWithNoOvertimePay() {
		payrollDetails.setEmployeeNum(1234);
		payrollDetails.setHourlyRate(2000);
		payrollDetails.setHoursWorked(10);
		Payroll newPayroll = new Payroll();
		assertTrue(newPayroll.getSalary(payrollDetails).getTotalPay() == 20000);
		assertTrue(newPayroll.getSalary(payrollDetails).getOvertimePay() == 0);

	}

	@Test
	public void getSalaryWithOvertimePay() {
		payrollDetails.setEmployeeNum(1234);
		payrollDetails.setHourlyRate(2000);
		payrollDetails.setHoursWorked(11);
		Payroll newPayroll = new Payroll();

		System.out.println(newPayroll.getSalary(payrollDetails).getTotalPay());
		System.out.println(newPayroll.getSalary(payrollDetails)
				.getOvertimePay());
		
		//Should be greater than base pay which is equal to 20000
		assertTrue(newPayroll.getSalary(payrollDetails).getTotalPay() > 20000);
		//Must not be null
		assertTrue(newPayroll.getSalary(payrollDetails).getOvertimePay() != 0);

	}
}
