package com.cloudsherpas.payroll.service;

import com.cloudsherpas.payroll.dto.PayrollDetails;


public class PayrollService {
	public PayrollDetails getSalary(PayrollDetails payrollDetails){
		new PayrollRulesService().getSalaryRules(payrollDetails);
		return payrollDetails;
	}
}
