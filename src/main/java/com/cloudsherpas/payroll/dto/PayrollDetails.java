package com.cloudsherpas.payroll.dto;

public class PayrollDetails {

	// data fields
	private int employeeNum; // Place holder for employee name
	private int hoursWorked; // Number of hours worked in a week
	private double hourlyRate; // Employee's hourly rate
	private double totalPay; // total hours worked plus the hourly rate
	private double overtimePay; // overtime pay

	
	public int getEmployeeNum() {
		return employeeNum;
	}

	public void setEmployeeNum(int employeeNum) {
		this.employeeNum = employeeNum;
	}

	public int getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(int hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	public double getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(double hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public double getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(double totalPay) {
		this.totalPay = totalPay;
	}

	public double getOvertimePay() {
		return overtimePay;
	}

	public void setOvertimePay(double overtimePay) {
		this.overtimePay = overtimePay;
	}
	
	
}
