package com.cloudsherpas.payroll.api;

import com.cloudsherpas.payroll.dto.PayrollDetails;
import com.cloudsherpas.payroll.service.PayrollService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Add your first API methods in this class, or you may create another class. In
 * that case, please update your web.xml accordingly.
 **/
@Api(name = "payrollRuleEngine", version = "v1", description = "Sample API for Payroll Module")
public class Payroll {

	@ApiMethod(name = "getSalary", path = "salary", httpMethod = ApiMethod.HttpMethod.POST)
	public PayrollDetails getSalary(PayrollDetails payrollDetails) {
		return new PayrollService().getSalary(payrollDetails);
	}

}
